'''
Jinsung Yoon (11/27/2018)
PATE-GAN
'''

#%% Packages
import tensorflow as tf
import numpy as np

# Logistic regression (Teacher)
from sklearn.linear_model import LogisticRegression

#%% Function Start

def PATE_GAN(X_train, Y_train, X_test, Y_test, epsilon, delta):
    
    # Reset the graph
    tf.reset_default_graph()
    
    #%% Data Preprocessing
    Y_train = np.reshape(Y_train, (len(Y_train),1))
    Y_test = np.reshape(Y_test,(len(Y_test),1))
       
    # Parameters
    X_dim = len(X_train[0,:])
    Train_No = len(X_train[:,0])
    Test_No = len(Y_test[:,0])
    
    #%% Data Normalization
    Min_Val = np.min(X_train,0)
    
    X_train = X_train - Min_Val
    
    Max_Val = np.max(X_train,0)
    
    X_train = X_train / (Max_Val + 1e-8)
    
    #%% Parameters
    # Batch size    
    mb_size = 128
    # Random variable dimension
    z_dim = int(X_dim/10)
    # Hidden unit dimensions
    h_dim = int(X_dim/5)
    # Conditioning dimension
    C_dim = 1
          
    # Noise parameters
    T_No = 10 
    lamda = 1
    L = 10
    Alpha = np.zeros([L,])
    
    #%% Data Division for Teachers
    Teacher_Data_X = []
    Teacher_Data_Y = []
    
    Teacher_Data_No = int(Train_No/T_No)
    
    idx = np.random.permutation(Train_No)
    for i in range(T_No):
        if (i < T_No - 1):
            Teacher_Data_X.append(X_train[idx[i*Teacher_Data_No:(i+1)*Teacher_Data_No],:])
            Teacher_Data_Y.append(Y_train[idx[i*Teacher_Data_No:(i+1)*Teacher_Data_No],:])
        else:
            Teacher_Data_X.append(X_train[idx[i*Teacher_Data_No:],:])
            Teacher_Data_Y.append(Y_train[idx[i*Teacher_Data_No:],:])
    
    #%% Algorithm Start
    Iter = 0
    #%% Necessary Functions

    # Xavier Initialization Definition
    def xavier_init(size):
        in_dim = size[0]
        xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
        return tf.random_normal(shape = size, stddev = xavier_stddev)    
        
    # Sample from uniform distribution
    def sample_Z(m, n):
        return np.random.uniform(-1., 1., size = [m, n])
        
    # Sample from the real data
    def sample_X(m, n):
        return np.random.permutation(m)[:n]  
     
    #%% Placeholder
    
    # Random Variable    
    Z = tf.placeholder(tf.float32, shape = [None, z_dim])
    # Conditional Variable
    C = tf.placeholder(tf.float32, shape = [None, C_dim])
    # Conditional Variable
    M = tf.placeholder(tf.float32, shape = [None, C_dim])
      
    #%% Student
    S_W1 = tf.Variable(xavier_init([X_dim + C_dim, h_dim]))
    S_b1 = tf.Variable(tf.zeros(shape=[h_dim]))

    S_W2 = tf.Variable(xavier_init([h_dim,1]))
    S_b2 = tf.Variable(tf.zeros(shape=[1]))

    theta_S = [S_W1, S_W2, S_b1, S_b2]
    
    #%% Generator
    G_W1 = tf.Variable(xavier_init([z_dim + C_dim, h_dim]))
    G_b1 = tf.Variable(tf.zeros(shape=[h_dim]))

    G_W2 = tf.Variable(xavier_init([h_dim,X_dim]))
    G_b2 = tf.Variable(tf.zeros(shape=[X_dim]))
    
    theta_G = [G_W1, G_W2, G_b1, G_b2]

    #%% Functions
    def generator(z, c):
        inputs = tf.concat(axis=1, values = [z,c])
        G_h1 = tf.nn.tanh(tf.matmul(inputs, G_W1) + G_b1)
        G_log_prob = tf.nn.sigmoid(tf.matmul(G_h1, G_W2) + G_b2)
        
        return G_log_prob
    
    def student(x,c):
        inputs = tf.concat(axis=1, values = [x,c])
        S_h1 = tf.nn.relu(tf.matmul(inputs,S_W1) + S_b1)
        out = tf.nn.sigmoid(tf.matmul(S_h1, S_W2) + S_b2)
        
        return out
    
    #%% 
    # Structure
    G_sample = generator(Z,C)
    S_fake = student(G_sample,C)

    #%%

    # Loss function
    S_loss = -tf.reduce_mean( M * tf.log(S_fake + 1e-8) + (1-M) * tf.log(1 - S_fake + 1e-8))
    G_loss = -tf.reduce_mean( tf.log(S_fake + 1e-8))

    # Solver
    S_solver = tf.train.AdamOptimizer().minimize(S_loss, var_list = theta_S)
    G_solver = tf.train.AdamOptimizer().minimize(G_loss, var_list = theta_G)
            
    #%% Sessions
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
        
    #%%
    # Iterations
    new_epsilon = 0
    
    while (new_epsilon < epsilon) :
      
      
        #%% Teacher Training            
        Z_mb = sample_Z(mb_size, z_dim)            
            
        # Teacher 1
        X_idx = sample_X(Train_No,mb_size)     
        C_mb = Y_train[X_idx,:]  
        
        G_mb = sess.run([G_sample], feed_dict = {Z: Z_mb, C: C_mb})[0]
        
        Teacher_Model = []
        
        for tt in range(T_No):
            Temp_Model = LogisticRegression()
            Temp_train_X1 = np.concatenate((Teacher_Data_X[tt],Teacher_Data_Y[tt]),axis = 1)
            Temp_train_X2 = np.concatenate((G_mb,C_mb),axis = 1) 
            Temp_train_X = np.concatenate((Temp_train_X1,Temp_train_X2), axis = 0)
            Temp_train_Y = np.concatenate((np.ones([len(Teacher_Data_Y[tt]),1]), np.zeros([mb_size,1])), axis = 0)
            
            Temp_Model.fit(Temp_train_X, Temp_train_Y)
            
            Teacher_Model.append(Temp_Model)
      
        #%% Student Training            
        for _ in range(5):
            Z_mb = sample_Z(mb_size, z_dim)            
            
            # Teacher 1
            X_idx = sample_X(Train_No,mb_size)        
            C_mb = Y_train[X_idx,:]  
            
            G_mb = sess.run([G_sample], feed_dict = {Z: Z_mb, C: C_mb})[0]
            
            temp_n = np.zeros([mb_size,2])
            temp_q = np.zeros([mb_size,1])
            
            for tt in range(T_No):
                temp = 1.*(Teacher_Model[tt].predict_proba(np.concatenate((G_mb,C_mb),axis=1))[:,1]>=0.5)
                temp_n[:,0] = temp_n[:,0] + (1-temp)
                temp_n[:,1] = temp_n[:,1] + (temp)
                
            laplace1 = np.random.laplace(loc=0.0, scale=lamda, size = mb_size)
            laplace0 = np.random.laplace(loc=0.0, scale=lamda, size = mb_size)
            R_mb = 1.*(temp_n[:,1]+laplace1 - temp_n[:,0]-laplace0 > 0)
            
            temp_q = (2+lamda * abs(temp_n[:,0]-temp_n[:,1]))/(4*np.exp(lamda*abs(temp_n[:,0]-temp_n[:,1]))+1e-8)
            
            for i in range(L):
                Temp1 = 2*(lamda**2)*(i+1)*(i+2)*np.ones([mb_size,])
                Temp2 = np.log((1-temp_q)*((1-temp_q)/(1-np.exp(2*lamda)*temp_q+1e-8))**(i+1) + temp_q * np.exp(2*lamda *(i+1)) + 1e-8)
                Alpha[i] = Alpha[i] + np.sum(np.minimum(Temp1,Temp2))                       
                        
            _, S_loss_curr = sess.run([S_solver, S_loss], feed_dict = {Z: Z_mb, C: C_mb, M: np.reshape(R_mb,[len(R_mb),1])})
            
                                        
        _, G_loss_curr = sess.run([G_solver, G_loss], feed_dict = {Z: Z_mb, C: C_mb, M: np.reshape(R_mb,[len(R_mb),1])})
        
        #%% Epsilon Epdate
        Temp = np.zeros([L,])
        for i in range(L):
            Temp[i] = Alpha[i]/(i+1)
        new_epsilon = np.min(Temp)

        Iter = Iter + 1
        
        print(new_epsilon)
    
    #%% Output Generation (Train Set)
    
    X_train_New = sess.run([G_sample], feed_dict = {Z: sample_Z(Train_No, z_dim), C: Y_train})

    X_train_New = X_train_New[0]
        
    #### Renormalization
        
    X_train_New = X_train_New * (Max_Val + 1e-8)
    
    X_train_New = X_train_New + Min_Val
    
    #### Y Return
    
    Y_train_New = np.reshape(Y_train, (len(Y_train[:,0]),))    
    
    #%% Output Generation (Test Set)
    
    X_test_New = sess.run([G_sample], feed_dict = {Z: sample_Z(Test_No, z_dim), C: Y_test})

    X_test_New = X_test_New[0]

    #### Renormalization
        
    X_test_New = X_test_New * (Max_Val + 1e-8)
    
    X_test_New = X_test_New + Min_Val

    #### Y Return
    
    Y_test_New = np.reshape(Y_test, (len(Y_test[:,0]),))    
    
    #%% Return
    
    return X_train_New, Y_train_New, X_test_New, Y_test_New